package be.jschoreels.demo.eventdriven.usermanagement.controller;

import be.jschoreels.demo.eventdriven.usermanagement.domain.Account;
import be.jschoreels.demo.eventdriven.usermanagement.event.AccountCreated;
import be.jschoreels.demo.eventdriven.usermanagement.event.AccountDeleted;
import be.jschoreels.demo.eventdriven.usermanagement.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

@RestController("/users")
public class UserManagementService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    void createUser(@RequestBody Account account){
        accountRepository.save(account);
        jmsTemplate.convertAndSend("VirtualTopic.User:AccountCreated", AccountCreated.newBuilder()
                .withUsername(account.getUsername())
                .withEmail(account.getEmail())
                .withPhoneNumber(account.getPhoneNumber())
                .build());
    }

    @RequestMapping(path = "/{username}", method = RequestMethod.DELETE)
    void deleteUser(@PathVariable("username") String username){
        accountRepository.deleteById(username);
        jmsTemplate.convertAndSend("VirtualTopic.User:AccountDeleted", AccountDeleted.newBuilder()
                .withUsername(username)
                .build());
    }
}
