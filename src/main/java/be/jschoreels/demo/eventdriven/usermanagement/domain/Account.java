package be.jschoreels.demo.eventdriven.usermanagement.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Id;


@JsonDeserialize(builder = Account.Builder.class)
@Entity
@Access(AccessType.FIELD)
public class Account {

    @Id
    private String username;
    private String phoneNumber;
    private String email;

    public Account() {}

    private Account(Builder builder) {
        username = builder.username;
        phoneNumber = builder.phoneNumber;
        email = builder.email;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(Account copy) {
        Builder builder = new Builder();
        builder.username = copy.getUsername();
        builder.phoneNumber = copy.getPhoneNumber();
        builder.email = copy.getEmail();
        return builder;
    }

    public String getUsername() {
        return username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPOJOBuilder
    public static final class Builder {
        private String username;
        private String phoneNumber;
        private String email;

        private Builder() {
        }

        public Builder withUsername(String val) {
            username = val;
            return this;
        }

        public Builder withPhoneNumber(String val) {
            phoneNumber = val;
            return this;
        }

        public Builder withEmail(String val) {
            email = val;
            return this;
        }

        public Account build() {
            return new Account(this);
        }
    }
}
