package be.jschoreels.demo.eventdriven.usermanagement.repository;

import be.jschoreels.demo.eventdriven.usermanagement.domain.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, String> {
}
